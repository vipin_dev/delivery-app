import React from 'react';

import { Header } from './components/header';
import { Main } from './components/main';
import { Footer } from './components/footer';
import { TripOrderList } from './components/tripOrderList';
import DeliveryCam from './components/deliveryCam';
import LiveCam from './components/liveCam';
import Login  from './components/login';
import SignIn  from './components/signin';

import cardItems  from './components/cardItems';
import OrderList  from './components/orderList';
import Ride from './components/ride';
import DeliveryDetails from './components/deliveryDetails';
import ModalItem from './components/modal';

import ReactDOM from 'react-dom';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    Redirect
  } from "react-router-dom";
import CamManager from './components/camManager';
export const App = () => {
 return (
    <Router>
    <div>
        {/* <Header /> */}
      <Switch>
        <Route exact path="/" component={SignIn} />
         <Route exact path="/trips" component={TripOrderList} />
         {/* <Route exact path="/cam" component={DeliveryCam} /> */}
         <Route exact path="/live" component={LiveCam} />
         <Route exact path="/login" component={Login} />
         <Route exact path="/signin" component={SignIn} />
         <Route exact path='/card' component={cardItems} />
         <Route exact path='/orderList' component={OrderList}/>
         <Route exact path='/ride' component={Ride} />
         <Route exact path='/delivery' component={DeliveryDetails} />
         {/* <Route exact path='/Modal' component={ModalItem} /> */}
        <Route exact path="/cam" component={CamManager} />
        {/* <Route exact path="/test3" component={Test3} /> */}
      </Switch>
    </div>
  </Router>
 );
};

export default App;