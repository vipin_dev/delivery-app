import React, { useState } from "react";
import "../assets/css/custom.scss";
import "../assets/css/style.bundle.scss";
function OrderDetails() {
  return (
    <div class="d-flex flex-column-fluid">
      <div class="container">
        <div
          class="card card-custom card-stretch example example-compact mt-5 order-card"
          id="kt_page_stretched_card"
          style={{ height: "auto" }}
        >
          <div
            class="card-header col-12 d-flex align-items-center p-4"
            kt-hidden-height="74"
          >
            <div className="col-3 order-item-text p-0 pl-2 pr-2">O-1234</div>

            <div
              className="col-5 order-item-text d-flex"
              style={{ borderLeft: "1px solid" }}
            >
              <div className="col-12 pl-0 pr-0" style={{ textAlign: " left" }}>
                108 Park Road Gate Avenue 878787
              </div>
            </div>

            <div className="col-4 order-item-text p-0">
              <div className="col-12">
                <img src="../../images/time.png" />
              </div>
              <div className="col-12 pt-2">2:00PM</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default OrderDetails;
