import React, { useState } from "react";
import SimpleMap from "./mapBlock";
import MapContainer from "./googleMap";
import Modal from "react-bootstrap/Modal";
import ModalBody from "react-bootstrap/ModalBody";
import ModalHeader from "react-bootstrap/ModalHeader";
import ModalFooter from "react-bootstrap/ModalFooter";
import ModalTitle from "react-bootstrap/ModalTitle";
import Button from 'react-bootstrap/Button';
// import DeliveryCam from './deliveryCam';
export const TripOrderList = () => {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const handleIndivitualOrder = (selectedOrder: any) => {
    console.log('here', selectedOrder);
    // setShow(!show)
    window.location.replace("http://localhost:3000/live");


  }
  return (
    <div className="album py-0 bg-light">
      <section className="text-center mb-0 bg-white">
   <div className="container">
    <h1 className="jumbotron-heading">Order List</h1>
    {/* <div className="alert alert-success" role="alert">fdg  fgfdgdf dfgdfg fdgdg</div> */}

    <p>
     <button  className="btn btn-primary m-2 active">
      Active Orders <span className="badge active"> (2)</span>
     </button>
     <button   className="btn btn-secondary m-2">
      Completed Orders (5)
     </button>
    </p>
   </div>
  </section>
      <div className="container">
        <div className="row">
        
          <ul className="list-group" style={{ width: "100%" }}>
            {[1, 2, 3, 4, 5, 6, 7].map((item) => {
              return (
                <li className="list-group-item d-flex m-1 justify-content-between align-items-center">
                  <div className="col-md-3">Order -#00-{item}</div>
                  <div className="col-md-3">
                    <svg
                      width="1em"
                      height="1em"
                      viewBox="0 0 16 16"
                      className="bi bi-geo-alt-fill"
                      fill="currentColor"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        fill-rule="evenodd"
                        d="M8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10zm0-7a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"
                      />
                    </svg>{" "}
                    : TXS Building <br />{" "}
                    <svg
                      width="1em"
                      height="1em"
                      viewBox="0 0 16 16"
                      className="bi bi-telephone-outbound-fill"
                      fill="currentColor"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        fill-rule="evenodd"
                        d="M1.885.511a1.745 1.745 0 0 1 2.61.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511zM11 .5a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-1 0V1.707l-4.146 4.147a.5.5 0 0 1-.708-.708L14.293 1H11.5a.5.5 0 0 1-.5-.5z"
                      />
                    </svg>{" "}
                    636-678-0987
                  </div>
                  <div className="col-md-2">2.30pm</div>
                  <div className="col-md-2">{item > 2 ? 'Completed': 'Pending'}</div>
                  <button
                    type="button"
                    className={`btn ${item > 2 ? "btn-outline-success" : "btn-outline-warning"} m-2`} 
                    // data-toggle="modal"
                    // data-target="#exampleModal"
                    onClick={() => handleIndivitualOrder(item)}
                  >
                    <span className={`badge ${item > 2 ? 'badge-success' : 'badge-warning'} badge-pill`}>5</span>
                    <svg
                      width="1em"
                      height="1em"
                      viewBox="0 0 16 16"
                      className="bi bi-check-all"
                      fill="currentColor"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        fill-rule="evenodd"
                        d="M8.97 4.97a.75.75 0 0 1 1.071 1.05l-3.992 4.99a.75.75 0 0 1-1.08.02L2.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093L8.95 4.992a.252.252 0 0 1 .02-.022zm-.92 5.14l.92.92a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 1 0-1.091-1.028L9.477 9.417l-.485-.486-.943 1.179z"
                      />
                    </svg>
                  </button>
                </li>
              );
            })}
          </ul>
          <div className="col-md-12">{/* <MapContainer /> */}</div>
          <div className="col-md-12">
            <SimpleMap />
          </div>
        </div>
      </div>
      <>
      <Modal show={show} onHide={handleIndivitualOrder}>
        <Modal.Header closeButton>
          <Modal.Title>Modal heading</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {/* <DeliveryCam /> */}
          </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleIndivitualOrder}>
            Close
          </Button>
          <Button variant="primary" onClick={handleIndivitualOrder}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
<div className="modal fade" id="exampleModal1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div className="modal-dialog" role="document">
    <div className="modal-content">
      <div className="modal-header">
        <h5 className="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div className="modal-body">
        {/* <DeliveryCam /> */}
      </div>
      <div className="modal-footer">
        <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" className="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
      </>
    </div>
  );
};
