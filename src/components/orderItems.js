import React, { useState } from "react";
import "../assets/css/custom.scss";
import "../assets/css/style.bundle.scss";
function OrderItems() {
  return (
    <div class="d-flex flex-column-fluid">
      <div class="container">
        <div
          class="card card-custom card-stretch example example-compact mt-5 order-card"
          id="kt_page_stretched_card"
          style={{height: 'auto'}}

        >
          <div
            class="card-header col-12 d-flex align-items-center p-2"
            kt-hidden-height="74"
            style={{ minHeight: '55px'}}
          >
            <div className="col-3 order-item-text p-0" style={{borderRight :'1px solid'}}>P-1100</div>
            <div className="col-4 order-item-text" style={{borderRight :'1px solid'}}>25 Orders</div>

            <div className="col-4 order-item-text d-flex m-0 pl-1">
              <div className="col-10 pl-0 pr-0" style={{marginRight : '-4px'}}>2:00 PM</div>
              <div className="col-2 pl-0 pr-0" style={{marginTop: '-1px'}}>
                <img src="../../images/time.png" />
              </div>
            </div>

            <div className="col-1 order-item-text p-0">
              <img src="../../images/pending.png" />
            </div>
          </div>
        </div>
        <div
          class="card card-custom card-stretch example example-compact mt-5 order-card"
          id="kt_page_stretched_card"
          style={{height: 'auto'}}

        >
          <div
            class="card-header col-12 d-flex align-items-center p-2"
            kt-hidden-height="74"
            style={{ minHeight: '55px'}}
          >
            <div className="col-3 order-item-text p-0" style={{borderRight :'1px solid'}}>P-1100</div>
            <div className="col-4 order-item-text" style={{borderRight :'1px solid'}}>25 Orders</div>

            <div className="col-4 order-item-text d-flex m-0 pl-1">
              <div className="col-10 pl-0 pr-0" style={{marginRight : '-4px'}}>2:00 PM</div>
              <div className="col-2 pl-0 pr-0" style={{marginTop: '-1px'}}>
                <img src="../../images/time.png" />
              </div>
            </div>

            <div className="col-1 order-item-text p-0">
              <img src="../../images/pending.png" />
            </div>
          </div>
        </div>
        <div
          class="card card-custom card-stretch example example-compact mt-5 order-card"
          id="kt_page_stretched_card"
          style={{height: 'auto'}}

        >
          <div
            class="card-header col-12 d-flex align-items-center p-2"
            kt-hidden-height="74"
            style={{ minHeight: '55px'}}
          >
            <div className="col-3 order-item-text p-0" style={{borderRight :'1px solid'}}>P-1100</div>
            <div className="col-4 order-item-text" style={{borderRight :'1px solid'}}>25 Orders</div>

            <div className="col-4 order-item-text d-flex m-0 pl-1">
              <div className="col-10 pl-0 pr-0" style={{marginRight : '-4px'}}>2:00 PM</div>
              <div className="col-2 pl-0 pr-0" style={{marginTop: '-1px'}}>
                <img src="../../images/time.png" />
              </div>
            </div>

            <div className="col-1 order-item-text p-0">
              <img src="../../images/completed.png" />
            </div>
          </div>
        </div>
        <div
          class="card card-custom card-stretch example example-compact mt-5 order-card"
          id="kt_page_stretched_card"
          style={{height: 'auto'}}

        >
          <div
            class="card-header col-12 d-flex align-items-center p-2"
            kt-hidden-height="74"
            style={{ minHeight: '55px'}}
          >
            <div className="col-3 order-item-text p-0" style={{borderRight :'1px solid'}}>P-1100</div>
            <div className="col-4 order-item-text" style={{borderRight :'1px solid'}}>25 Orders</div>

            <div className="col-4 order-item-text d-flex m-0 pl-1">
              <div className="col-10 pl-0 pr-0" style={{marginRight : '-4px'}}>2:00 PM</div>
              <div className="col-2 pl-0 pr-0" style={{marginTop: '-1px'}}>
                <img src="../../images/time.png" />
              </div>
            </div>

            <div className="col-1 order-item-text p-0">
              <img src="../../images/completed.png" />
            </div>
          </div>
        </div>
        <div
          class="card card-custom card-stretch example example-compact mt-5 order-card"
          id="kt_page_stretched_card"
          style={{height: 'auto'}}

        >
          <div
            class="card-header col-12 d-flex align-items-center p-2"
            kt-hidden-height="74"
            style={{ minHeight: '55px'}}
          >
            <div className="col-3 order-item-text p-0" style={{borderRight :'1px solid'}}>P-1100</div>
            <div className="col-4 order-item-text" style={{borderRight :'1px solid'}}>25 Orders</div>

            <div className="col-4 order-item-text d-flex m-0 pl-1">
              <div className="col-10 pl-0 pr-0" style={{marginRight : '-4px'}}>2:00 PM</div>
              <div className="col-2 pl-0 pr-0" style={{marginTop: '-1px'}}>
                <img src="../../images/time.png" />
              </div>
            </div>

            <div className="col-1 order-item-text p-0">
              <img src="../../images/completed.png" />
            </div>
          </div>
        </div>
        <div
          class="card card-custom card-stretch example example-compact mt-5 order-card"
          id="kt_page_stretched_card"
          style={{height: 'auto'}}

        >
          <div
            class="card-header col-12 d-flex align-items-center p-2"
            kt-hidden-height="74"
            style={{ minHeight: '55px'}}
          >
            <div className="col-3 order-item-text p-0" style={{borderRight :'1px solid'}}>P-1100</div>
            <div className="col-4 order-item-text" style={{borderRight :'1px solid'}}>25 Orders</div>

            <div className="col-4 order-item-text d-flex m-0 pl-1">
              <div className="col-10 pl-0 pr-0" style={{marginRight : '-4px'}}>2:00 PM</div>
              <div className="col-2 pl-0 pr-0" style={{marginTop: '-1px'}}>
                <img src="../../images/time.png" />
              </div>
            </div>

            <div className="col-1 order-item-text p-0">
              <img src="../../images/completed.png" />
            </div>
          </div>
        </div>
        <div
          class="card card-custom card-stretch example example-compact mt-5 order-card"
          id="kt_page_stretched_card"
          style={{height: 'auto'}}

        >
          <div
            class="card-header col-12 d-flex align-items-center p-2"
            kt-hidden-height="74"
            style={{ minHeight: '55px'}}
          >
            <div className="col-3 order-item-text p-0" style={{borderRight :'1px solid'}}>P-1100</div>
            <div className="col-4 order-item-text" style={{borderRight :'1px solid'}}>25 Orders</div>

            <div className="col-4 order-item-text d-flex m-0 pl-1">
              <div className="col-10 pl-0 pr-0" style={{marginRight : '-4px'}}>2:00 PM</div>
              <div className="col-2 pl-0 pr-0" style={{marginTop: '-1px'}}>
                <img src="../../images/time.png" />
              </div>
            </div>

            <div className="col-1 order-item-text p-0">
              <img src="../../images/completed.png" />
            </div>
          </div>
        </div>
      </div>
      
    </div>
  );
}

export default OrderItems;
