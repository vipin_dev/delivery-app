import React, { useState, useEffect } from "react";
import "../assets/css/custom.scss";
import "../assets/css/style.bundle.scss";
function CamManager(props) {
  var btnStart = null;
  var btnStop = null;
  var btnCapture = null;

  // The stream & capture
  var stream = null;
  var capture = null;
  var snapshot = null;

  // The video stream
  var cameraStream = null;

  useEffect(() => {
    btnStart = document.getElementById("btn-start");
    btnStop = document.getElementById("btn-stop");
    btnCapture = document.getElementById("btn-capture");

    // The stream & capture
    stream = document.getElementById("stream");
    capture = document.getElementById("capture");
    snapshot = document.getElementById("snapshot");
    startStreaming();
    // The video stream
    cameraStream = null;

    // Attach listeners
    if (btnStart) {
      btnStart.addEventListener("click", startStreaming);
    }

    if (btnStop) {
      btnStop.addEventListener("click", stopStreaming);
    }
    if (btnCapture) {
      btnCapture.addEventListener("click", captureSnapshot);
    }
  }, []);

  // Start Streaming
  function startStreaming() {
    console.log("here start...");
    var mediaSupport = "mediaDevices" in navigator;

    if (mediaSupport && null == cameraStream) {
      navigator.mediaDevices
        .getUserMedia({ video: true })
        .then(function (mediaStream) {
          cameraStream = mediaStream;

          stream.srcObject = mediaStream;

          stream.play();
        })
        .catch(function (err) {
          console.log("Unable to access camera: " + err);
        });
    } else {
      alert("Your browser does not support media devices.");

      return;
    }
  }

  // Stop Streaming
  function stopStreaming() {
    if (null != cameraStream) {
      var track = cameraStream.getTracks()[0];

      track.stop();
      stream.load();

      cameraStream = null;
    }
    props.handleModalEvent();
  }

  function captureSnapshot() {
    if (null != cameraStream) {
      var ctx = capture.getContext("2d");
      var img = new Image();

      ctx.drawImage(stream, 0, 0, capture.width, capture.height);

      img.src = capture.toDataURL("image/png");
      img.width = 240;

      snapshot.innerHTML = "";

      snapshot.appendChild(img);
      capture.style = "display:none";
      console.log('obj', img.src);
      props.handleUploadImage(img);
      stopStreaming()
    }
  }

  // The buttons to start & stop stream and to capture the image

  return (
    <>
      <div className="play-area">
        <div className="play-area-sub">
          <h3>Live</h3>
          <video id="stream" width="320" height="240"></video>
        </div>
      </div>

      
      {/* <div class="modal-footer"><button type="button" class="btn btn-secondary">Close</button><button type="button" class="btn btn-primary">Save Changes</button></div> */}
      <div className="button-group">
        {/* <button id="btn-start" type="button" className="button">Start</button> */}
        <button id="btn-stop" type="button" className="btn btn-danger mr-5">
          Stop
        </button>
        <button id="btn-capture" type="button" className="btn btn-secondary">
          Capture Image
        </button>
      </div>
      <div className="play-area-sub">
        {/* <h3>Preview</h3> */}
        <canvas id="capture" width="320" height="240" style={{visibility: 'hidden'}}></canvas>
        <div id="snapshot"></div>
      </div>
    </>
  );
}

export default CamManager;
