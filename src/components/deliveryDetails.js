import React, { useState } from "react";
import "../assets/css/custom.scss";
import "../assets/css/style.bundle.scss";
import DeaderOptionsDelivery from "../components/headerOptionsDelivery";
import DeliveryImageTool from "../components/deliveryImageTool";
function DeliveryDetails() {
  return (
    <>
      <DeaderOptionsDelivery />

      <div className="row">
        <div className="col-lg-12 delivery-header-label pl-10">
          Order Details
        </div>

        <div
          className="col-lg-12 order-details-block m-5 d-flex p-5"
          style={{ minHeight: "50px" }}
        >
          <div className="col-lg-6">
            <div className="col-lg-12 delivery-label-header p-0">O-1234</div>
            <div className="col-lg-12 d-flex p-0 delivery-info">
              <div className="col-lg-6 p-0">Total items</div>
              <div className="col-lg-6 d-flex p-0 justify-content-end">12</div>
            </div>
            <div className="col-lg-12 d-flex p-0 delivery-info">
              <div className="col-lg-6 p-0">Price</div>
              <div className="col-lg-6 d-flex p-0 justify-content-end">200</div>
            </div>
          </div>
          <div className="col-lg-6">
            <div className="col-lg-12 delivery-label-header">Delivered To:</div>
            <div className="col-lg-12 delivery-address delivery-info">
              108 Park Road 
              Gate Avenue
              878787
            </div>
          </div>
        </div>
      </div>
      {/* <div className="row m-0 ml-10"> */}
          <DeliveryImageTool />
          
      {/* </div> */}
      <div className="row p-4 justify-content-space-around align-items-center">
        <div className="col-5 star-rating-label">
        Provide Rating
        </div>
        <div className="col-7">
          <img src="../../images/star.png"/>
          <img src="../../images/star.png"/>
          <img src="../../images/star.png"/>
          <img src="../../images/star.png"/>
          <img src="../../images/star.png"/>
        </div>
      </div>
      {/* <StarRatings /> */}
    </>
  );
}
export default DeliveryDetails;
