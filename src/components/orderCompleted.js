import React from "react";
import Webcam from "react-webcam";

const WebcamCapture = () => {
    const webcamRef = React.useRef(null);
    const [imgSrc, setImgSrc] = React.useState(null);
  
    const capture = React.useCallback(() => {
      const imageSrc = webcamRef.current.getScreenshot();
      setImgSrc(imageSrc);
    }, [webcamRef, setImgSrc]);

    const reCapture = React.useCallback(() => {
        setImgSrc(null);
      }, [webcamRef, setImgSrc]);
  
    return (
      <>
      <div>
      {imgSrc == null && <Webcam
          audio={false}
          ref={webcamRef}
          screenshotFormat="image/jpeg"
        />}
        {imgSrc && (
          <img
            src={imgSrc}
          />
        )}
      </div>
       <div className="row">
       {imgSrc == null && <button onClick={capture}>Capture photo</button>}
        {imgSrc && <button onClick={reCapture}>Capture Again</button>}
      
       </div>
        </>
    );
  };
  export default WebcamCapture;