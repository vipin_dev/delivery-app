import React, { useState } from "react";
import "../assets/css/custom.scss";
import "../assets/css/style.bundle.scss";
import "../assets/css/deliveryapp.scss";
function HeaderOptions() {
  return (
    <div class="d-flex flex-column-fluid">
      <div class="container mt-5">
        <div
          class="card card-custom card-stretch example example-compact"
          id="kt_page_stretched_card"
        >
          <div
            class="card-header col-12 d-flex align-items-center pl-0 pr-0"
            kt-hidden-height="74"
          >
            <div className="col-8 pl-0">
              <div className="col-12 header-info pl-0">Welcome</div>
              <div className="col-12 header-user-name pl-0">Nolan</div>
            </div>
            <div className="col-4 d-flex align-items-center">
              <div className="col-6">
                <img src="../../images/Vector.png" />
              </div>
              <div className="col-6">
                <img src="../../images/Ellipse 3.png" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default HeaderOptions;
