import React from "react";

export const TripHistory = () => {
  return (
    <ul className="list-group" style={{ width: "100%" }}>
      {[1, 2, 3, 4, 5, 6, 7].map((item) => {
        return (
          <li className="list-group-item d-flex m-1 justify-content-between align-items-center">
            Trip #TR-000{item}
            <button type="button" className="btn btn-outline-success">
              <span className="badge badge-secondary badge-pill">14</span>
              <svg
                width="1em"
                height="1em"
                viewBox="0 0 16 16"
                className="bi bi-check-all"
                fill="currentColor"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fill-rule="evenodd"
                  d="M8.97 4.97a.75.75 0 0 1 1.071 1.05l-3.992 4.99a.75.75 0 0 1-1.08.02L2.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093L8.95 4.992a.252.252 0 0 1 .02-.022zm-.92 5.14l.92.92a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 1 0-1.091-1.028L9.477 9.417l-.485-.486-.943 1.179z"
                />
              </svg>
            </button>
          </li>
        );
      })}
    </ul>
  );
};
