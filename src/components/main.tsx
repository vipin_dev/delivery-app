import React, {useState} from "react";

import { Jumbotron } from "./jumbotron";
import { Card } from "./card";
import { List } from "./tripList";
import TileTrip from "./tileTrip";
import { TripTabs } from "./tripTabs";
import { TripHistory } from './tripHistory';

export const Main = () => {
    const [tripList, setTripList] = useState('active');
  
    const handleTripList = (status: any) => {
      console.log('here');
        if (status == 'active') {
            setTripList('active')
        } else {
            setTripList('history')
        }

    }
  return (
    <main role="main">
      <section className="text-center mb-0 bg-white">
        <div className="container">
          <h1 className="jumbotron-heading">Delivery App</h1>
          {/* <div className="alert alert-success" role="alert">fdg  fgfdgdf dfgdfg fdgdg</div> */}
{/* 
          <p>

            <button type="button" className={`btn btn-outline-warning m-2 ${tripList == 'active' ? "active" : ""}`} onClick={() => handleTripList('active')}>
              Active Trips <span className="badge active"> (1)</span>
            </button>
            <button className={`btn btn-outline-success m-2 ${tripList == 'history' ? "active" : ""}`} onClick={() => handleTripList('history')}>
              Completed Trips (24)
            </button>
          </p> */}
          
        </div>
        
      </section>
      <div className="album py-0 bg-light">
        <div className="container4">
          <div className="row m-10" style={{ margin :'10px'}}>
          <ul className="nav nav-pills" id="pills-tab" role="tablist">
  <li className="nav-item">
    <a className={tripList == 'active' ? 'nav-link active' : 'nav-link'} id="pills-home-tab"  aria-controls="pills-home" aria-selected="true" onClick={() => handleTripList('active')}> Active Trips <span className="badge active"> (1)</span></a>
  </li>
  <li className="nav-item">
    <a className={tripList == 'history' ? 'nav-link active' : 'nav-link'} id="pills-profile-tab" onClick={() => handleTripList('history')} aria-controls="pills-profile" aria-selected="false">Completed Trips (24)</a>
  </li>
</ul>


          </div>
          <div className="row">
            {tripList == 'active' && <TileTrip />}
            {tripList == 'history' && <TripHistory />}
          </div>
        </div>
      </div>
    </main>
  );
};
