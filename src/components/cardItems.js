import React, { useState } from "react";
import "../assets/css/custom.scss";
import "../assets/css/style.bundle.scss";
import HeaderOptions from "../components/headerOptions";
import OrderItems from '../components/orderItems';
function CardItems() {
  return (
    <>
      <HeaderOptions />
      <div className="row d-flex align-items-center">
        <div className="col-10 order-header pl-10">Today’s Orders</div>
        <div className="col-2"><img src="../../images/sort.png" /></div>
      </div>
      <OrderItems />
      {false &&
      <div class="d-flex flex-column-fluid">
        <div class="container mt-5">
          <div
            class="card card-custom card-stretch example example-compact"
            id="kt_page_stretched_card"
          >
            <div class="card-header p-0" kt-hidden-height="74">
              <div class="card-title">
                <h3 class="card-label">
                  Stretched Card{" "}
                  <small>full height card with scrollable content</small>
                </h3>
              </div>
            </div>
            <div class="card-body">
              <div class="card-scroll" style={{ height: "98px" }}>
                <div class="example-code mb-5">
                  <ul class="example-nav nav nav-tabs nav-bold nav-tabs-line nav-tabs-line-2x">
                    <li class="nav-item">
                      <a
                        class="nav-link active"
                        data-toggle="tab"
                        href="#example_code_html"
                      >
                        HTML
                      </a>
                    </li>
                    <li class="nav-item">
                      <a
                        class="nav-link"
                        data-toggle="tab"
                        href="#example_code_js"
                      >
                        JS
                      </a>
                    </li>
                  </ul>
                </div>

                <p>
                  Contrary to popular belief, Lorem Ipsum is not simply random
                  text. It has roots in a piece of classical Latin literature
                  from 45 BC, making it over 2000 years old. Richard McClintock,
                  a Latin professor at Hampden-Sydney College in Virginia,
                  looked up one of the more obscure Latin words, consectetur,
                  from a Lorem Ipsum passage, and going through the cites of the
                  word in classical literature, discovered the undoubtable
                  source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of
                  "de Finibus Bonorum et Malorum" (The Extremes of Good and
                  Evil) by Cicero, written in 45 BC. This book is a treatise on
                  the theory of ethics, very popular during the Renaissance. The
                  first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..",
                  comes from a line in section 1.10.32.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>}
    </>
  );
}

export default CardItems;
