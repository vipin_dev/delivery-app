import React from "react";
import Webcam from "react-webcam";
import CompletedList from "./completedList";
import { CustomInput, Form, FormGroup, Label } from "reactstrap";
import { Redirect, useHistory } from "react-router-dom";
import ListGroup from "react-bootstrap/ListGroup";

import "./live.scss";
const WebcamCapture = () => {
  let history = useHistory();
  const webcamRef = React.useRef(null);
  const [imgSrc, setImgSrc] = React.useState(null);
  const [camSet, setCamSet] = React.useState(0);

  const capture = React.useCallback(() => {
    const imageSrc = webcamRef.current.getScreenshot();
    setImgSrc(imageSrc);
  }, [webcamRef, setImgSrc]);

  const reCapture = React.useCallback(() => {
    setImgSrc(null);
  }, [webcamRef, setImgSrc]);

  const takePhoto = React.useCallback(() => {
    setCamSet(1);
  }, [webcamRef, setCamSet]);

  const tripCompleted = React.useCallback(() => {
    // return  <Redirect  to="/posts/" />
    history.push("/trips");
  });
  return (
    <div className="container">
      <div className="col-md-12" style={{ display: "flex" }}>
        <div className="col-sm-8 col-md-7 py-4">
          <div className="col-md-12">
            <ListGroup>
              <ListGroup.Item variant="primary">Order -#00-1</ListGroup.Item>
              <ListGroup.Item action variant="secondary">
              <div className="col-md-12">
                    <svg
                      width="1em"
                      height="1em"
                      viewBox="0 0 16 16"
                      className="bi bi-geo-alt-fill"
                      fill="currentColor"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        fill-rule="evenodd"
                        d="M8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10zm0-7a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"
                      />
                    </svg>{" "}
                    : TXS Building <br />{" "}
                    <svg
                      width="1em"
                      height="1em"
                      viewBox="0 0 16 16"
                      className="bi bi-telephone-outbound-fill"
                      fill="currentColor"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        fill-rule="evenodd"
                        d="M1.885.511a1.745 1.745 0 0 1 2.61.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511zM11 .5a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-1 0V1.707l-4.146 4.147a.5.5 0 0 1-.708-.708L14.293 1H11.5a.5.5 0 0 1-.5-.5z"
                      />
                    </svg>{" "}
                    :636-678-0987
                  </div>
              </ListGroup.Item>
              
            </ListGroup>
            <small id="emailHelp" className="form-text text-muted">
              Please take a order package/location photo.
            </small>
          </div>
          <div className="row videoFrame" style={{ display: "block" }}>
            

            {/* <Form>
              <FormGroup>
                <Label for="exampleCheckbox">Order Items</Label>
                <div>
                  <CustomInput
                    type="checkbox"
                    id="exampleCustomCheckbox"
                    label="Item1 * 1"
                  />
                  <CustomInput
                    type="checkbox"
                    id="exampleCustomCheckbox2"
                    label="Item2 * 1"
                  />
                </div>
              </FormGroup>
            </Form> */}
            {camSet == 1 && (
              <>
                <div className="row">
                  {imgSrc == null && (
                    <Webcam
                      audio={false}
                      ref={webcamRef}
                      screenshotFormat="image/jpeg"
                    />
                  )}
                  {imgSrc && <img src={imgSrc} />}
                </div>
                <div className="col-md-12" style={{ display: "flex" }}>
                  {imgSrc == null && (
                    <button
                      type="button"
                      style={{ marginTop: "20px" }}
                      class="btn btn-success"
                      onClick={capture}
                    >
                      Capture photo
                    </button>
                  )}
                </div>
              </>
            )}
            <div className="col-md-12 p-5" style={{ display: "flex" }}>
              {imgSrc && (
                <button
                  type="button"
                  class="btn btn-primary"
                  onClick={reCapture}
                >
                  Capture Again
                </button>
              )}

              {camSet == 0 && (
                <button
                  type="button"
                  class="btn btn-secondary"
                  onClick={takePhoto}
                >
                  Attach Photo
                </button>
              )}

              {imgSrc && camSet == 1 && (
                <button
                  style={{ marginLeft: "20px" }}
                  type="button"
                  class="btn btn-success"
                  onClick={tripCompleted}
                >
                  Trip Completed
                </button>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default WebcamCapture;
