import React from "react";
import { BrowserRouter as Router, Redirect, useHistory, Route, Link } from "react-router-dom";

const TileTrip = () => {
  let history = useHistory();

  const handleOrderPage = () => {
    history.push("/trips");
  };
  return (
    <Router><ul className="list-group" style={{ width: "100%" }}>
      <li className="list-group-item d-flex m-1 justify-content-between align-items-center" onClick={handleOrderPage}>
        
        <div className="col-md-2 col-sm-2 col-xs-2 col-lg-2"><img src="https://gepard.io/img/blog/1577449186-product_images_example4.jpg" alt="..." className="rounded" style={{width: '100%'}}/></div>
        <div className="col-md-10 col-sm-10 col-xs-10">
        <div className="row">
          <div className="col-md-12 col-sm-12 col-xs-12" style={{fontWeight: 'bold'}}>Trip #TR-000128</div>
        </div>
        <div className="row">
          <div className="col-md-12 col-sm-12 col-xs-12">
          <div className="alert-warning" role="alert">
            Pending</div></div>
        </div>
        <div className="row">
          <div className="col-md-12 col-sm-12 col-xs-12">
            15 Orders</div>
        </div>
        <div className="row">
          <div className="col-md-12 col-sm-12 col-xs-12">12/11/2020 3.30PM</div>
        </div>
        </div>
        
        
        
      </li>
      <li className="list-group-item d-flex m-1 justify-content-between align-items-center" onClick={handleOrderPage}>
        
        <div className="col-md-2 col-sm-2 col-xs-2 col-lg-2"><img src="https://gepard.io/img/blog/1577449186-product_images_example4.jpg" alt="..." className="rounded" style={{width: '100%'}}/></div>
        <div className="col-md-10 col-sm-10 col-xs-10">
        <div className="row">
          <div className="col-md-12 col-sm-12 col-xs-12" style={{fontWeight: 'bold'}}>Trip #TR-000128</div>
        </div>
        <div className="row">
          <div className="col-md-12 col-sm-12 col-xs-12">
          <div className="alert-warning" role="alert">
            Pending</div></div>
        </div>
        <div className="row">
          <div className="col-md-12 col-sm-12 col-xs-12">
            15 Orders</div>
        </div>
        <div className="row">
          <div className="col-md-12 col-sm-12 col-xs-12">12/11/2020 3.30PM</div>
        </div>
        </div>
        
        
        
      </li>
      {false && <li className="list-group-item d-flex m-1 justify-content-between align-items-center">
        Cras justo odio
        <button type="button" className="btn btn-outline-warning">
          <span className="badge badge-warning badge-pill">14</span>
          <svg
            width="1em"
            height="1em"
            viewBox="0 0 16 16"
            className="bi bi-chevron-double-right"
            fill="currentColor"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              fill-rule="evenodd"
              d="M3.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L9.293 8 3.646 2.354a.5.5 0 0 1 0-.708z"
            ></path>
            <path
              fill-rule="evenodd"
              d="M7.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L13.293 8 7.646 2.354a.5.5 0 0 1 0-.708z"
            ></path>
          </svg>
        </button>
      </li>}
    </ul></Router>
  );
};

export default TileTrip;

