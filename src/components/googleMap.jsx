import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';

const AnyReactComponent = ({ text }) => <div>{text} lll</div>;

class SimpleMap extends Component {
  static defaultProps = {
    center: {
      lat: -34,
      lng: 151
    },
    zoom: 11
  };

  render() {
    return (
      // Important! Always set the container height explicitly
      <div style={{ height: '100vh', width: '100%' }}>
        <GoogleMapReact
          bootstrapURLKeys={{ key: 'AIzaSyAZuoEv8cLbvdD1WU6Yj_9aVvqI3aCikc4' }}
          defaultCenter={this.props.center}
          defaultZoom={this.props.zoom}
        >
          <AnyReactComponent
            lat={-34}
            lng={151}
            text="My Marker"
          />
<AnyReactComponent
            lat={-34}
            lng={151}
            text="My Marker2"
          />
        </GoogleMapReact>
      </div>
    );
  }
}

export default SimpleMap;