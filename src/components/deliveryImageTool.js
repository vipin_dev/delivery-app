import React, { useState } from "react";
import "../assets/css/custom.scss";
import "../assets/css/style.bundle.scss";
import "../assets/css/deliveryapp.scss";
import ModalItem from "../components/modal";

function DeliveryImageTool() {
  const [uploadImages, setUploadImages] = useState([]);

  function handleUploadImage(imageSrc) {
    setUploadImages((uploadImages) => [...uploadImages, imageSrc.src]);
    console.log("here array data", uploadImages);
  }
  return (
    <>
      <div className="row image-preview-box d-flex pl-5">
        {uploadImages &&
          uploadImages.map((item) => (
            <div
            key={Math.random}
              className="delivery-image-box delivery-box-bg img-wrap"
              style={{ background: `url(${item})`,  }}
            >
              <span className="close">&times;</span>
            </div>
          ))}

        <ModalItem handleUploadImage={handleUploadImage} />
      </div>
      <div className="row">
      {uploadImages && uploadImages.length > 1 &&
        <div className="col-12 d-flex justify-content-center p-5 ml-25">
          <div
            className="col-1"
            style={{ textAlign: "end", padding: "10px 6px" }}
          >
            <img src="../../images/completed.png" />
          </div>
          
          <div className="col-11">
            {" "}
            <button type="button" class="btn btn-success delivery-action-btn">
              <i class="fa fa-trash"></i>Completed
            </button>
          </div>
        </div>}
        <div className="row"></div>
      </div>
    </>
  );
}

export default DeliveryImageTool;
