import React from 'react'
import StarRating from 'react-star-rating'

function StarRatings() {
    return (
        <>        <StarRating name="react-star-rating" caption="Rate this component!" totalStars={5} />
        </>
    );

  }
 

export default StarRatings;