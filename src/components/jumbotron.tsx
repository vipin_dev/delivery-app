// /\* eslint-disable jsx-a11y/anchor-is-valid \*/
import React from 'react';

export const Jumbotron = () => {
 return (
  <section className="text-center mb-0 bg-white">
   <div className="container">
    <h1 className="jumbotron-heading">Delivery App</h1>
    {/* <div className="alert alert-success" role="alert">fdg  fgfdgdf dfgdfg fdgdg</div> */}

    <p>
     <button  className="btn btn-primary m-2 active">
      Active Trips <span className="badge active"> (1)</span>
     </button>
     <button   className="btn btn-secondary m-2">
      Completed Trips (24)
     </button>
    </p>
   </div>
  </section>
 );
};