import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';
import {
    GoogleMapProvider,
    HeatMap,
    InfoWindow,
    MapBox,
    Marker,
    OverlayView,
    Polygon,
  } from '@googlemap-react/core'
const AnyReactComponent = (text : any) => <div>{text}</div>;
 
class SimpleMap extends Component {
  static defaultProps = {
    center: {
      lat: -34,
      lng: 151
    },
    zoom: 4
  };
 
  render() {
    return (
        <GoogleMapProvider>
          <MapBox
            apiKey="AIzaSyAZuoEv8cLbvdD1WU6Yj_9aVvqI3aCikc4"
            opts={{
              center: {lat: -34, lng: 151},
              zoom: 10,
            }}
            style={{
              height: '100vh',
              width: '100%',
            }}
            useDrawing
            useGeometry
            usePlaces
            useVisualization
            onCenterChanged={() => {
              console.log('The center of the map has changed.')
            }}
          />
          <Marker
            id="marker"
            opts={{
              draggable: true,
              label: 'here first place',
              position: {lat: -34, lng: 151},
            }}
          />
          <Marker
            id="marker1"
            opts={{
              draggable: true,
              label: 'here first place',
              position: {lat: -34, lng: 151},
            }}
          />
          <Polygon
            id="polygon"
            opts={{
              path: [
                {lat: 38.98, lng: 116.01},
                {lat: 38.98, lng: 116.03},
                {lat: 38.99, lng: 116.03},
              ],
              strokeColor: 'cyan',
            }}
          />
          <HeatMap
            opts={{
              data: [
                {lat: 38.982, lng: 116.037},
                {lat: 38.982, lng: 116.035},
                {lat: 38.985, lng: 116.047},
                {lat: 38.985, lng: 116.045},
              ],
            }}
          />
          {/* <OverlayView position={{lat: 39, lng: 116}}>
            <h2>⚑ This is a custom overlay 🙌</h2>
          </OverlayView> */}
        </GoogleMapProvider>
      )
  }
}
 
export default SimpleMap;