import React, {useState} from 'react'
import { Button,Modal } from 'react-bootstrap'
import CamManager from '../components/camManager';
function ModalItem(props) {

    const [showHide, setShowHide] = useState(false);

    
    const handleModalShowHide = () => {
        setShowHide(!showHide)
    }

    const handleUploadImageData = (data) => {
        console.log('proccc dtat', data);
        handleModalShowHide();
        props.handleUploadImage(data);
    }

        return(
            <>
            <div className="colh-3 delivery-image-box upload" onClick={() => handleModalShowHide()}>

                <div >
                    <div className="col-12">
                    <img src="../../images/upload.png" />

                    </div>
                    
                </div>

                

            </div>
            <Modal show={showHide}>
                    <Modal.Header closeButton onClick={() => handleModalShowHide()}>
                    <Modal.Title>Attach Photo</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                    <CamManager handleUploadImage={handleUploadImageData} handleModalEvent={() => handleModalShowHide()} />
                    </Modal.Body>
                    {/* <Modal.Footer>
                    <Button variant="secondary" onClick={() => handleModalShowHide()}>
                        Close
                    </Button>
                    <Button variant="primary" onClick={() => handleModalShowHide()}>
                        Save Changes
                    </Button>
                    </Modal.Footer> */}
                </Modal>
                </>
            
        );
    
}

export default ModalItem;