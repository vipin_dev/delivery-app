import React from 'react';
import { CustomInput, Form, FormGroup, Label } from 'reactstrap';

const CompletedList = (props) => {
  return (
    <Form>
      <FormGroup>
        <Label for="exampleCheckbox">Order Items</Label>
        <div>
          <CustomInput type="checkbox" id="exampleCustomCheckbox" label="Item1 * 1" />
          <CustomInput type="checkbox" id="exampleCustomCheckbox2" label="Item2 * 1" />
        </div>
      </FormGroup>
    </Form>
  );
}

export default CompletedList;