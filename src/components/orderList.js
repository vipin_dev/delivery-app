import React, { useState } from "react";
import "../assets/css/custom.scss";
import "../assets/css/style.bundle.scss";
import HeaderOptionsBack from "../components/headerOptionsBack";
import OrderDetails from '../components/orderDetails';
function OrderList() {
  return (
    <>
      <HeaderOptionsBack />
      <div className="row d-flex align-items-center">
        <div className="col-10 order-header pl-12">P-1100 Orders</div>
        <div className="col-2"><img src="../../images/sort.png" /></div>
      </div>
      <OrderDetails />


      </>);
}
export default OrderList;